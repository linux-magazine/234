require 'animation'
local time = 0

function love.load ()
  love.graphics.setBackgroundColor(0.5, 0.8, 1, 1)
  love.window.setTitle ("Cubey McCubeFace goes Walkies")
  
  cmcf = {}  
  cmcf.walk = animation ("images/cmcf.png", {64, 64}, 0.7, {100, 100})  
end

function love.update (dt)
  cmcf.walk.frame, cmcf.walk.time = framecounter (cmcf.walk.time + dt, cmcf.walk.duration)
end

function love.draw ()
  love.graphics.draw (cmcf.walk.reel, cmcf.walk.frames[cmcf.walk.frame], unpack (cmcf.walk.pos))
end