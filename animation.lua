function animation (reel, framesize, duration, pos)
  local animation = {}
     
  animation.reel = love.graphics.newImage (reel)
  
  animation.frames = {}
  for i = 0, animation.reel:getWidth() - framesize[1], framesize [1] do
    table.insert (animation.frames, love.graphics.newQuad (i, 0, framesize[1], framesize[2], animation.reel:getDimensions()))
  end
  
  animation.duration = duration
  animation.frame = 1
  animation.time = 0
  
  animation.pos = pos
  
  return animation
end

 function framecounter (time, duration)
  if time >= duration then
    time = 0
  end  
  return math.floor ((time / cmcf.walk.duration) * #cmcf.walk.frames) + 1, time
end